package Models

import com.google.gson.annotations.SerializedName

data class ResponseResult (
    val success: Boolean,
    val info: String,
    val token:String
)



