package com.marce.login_kotlin

import Interfases.ApiService
import Models.Login
import Models.ResponseResult
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.marce.login_kotlin.databinding.ActivityMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        clicks()
    }



    private fun clicks() {
      binding.btnI.setOnClickListener {
          validarLogin()
          obtenerInicioSesion()

      }
    }


    private fun validarLogin(): Boolean {
        var Valido = true

        if (TextUtils.isEmpty(binding.edtE.text.toString())) {
            // Si la propiedad error tiene no valor, se muestra el aviso de error
            binding.edtE.error = "Requerido"
            Valido = false
            Toast.makeText(this@MainActivity, "Correo vacio", Toast.LENGTH_LONG).show()



        } else binding.edtE.error = null

        //////  password   //////

        if (TextUtils.isEmpty((binding.edtP.text.toString()))){
            binding.edtP.error = "Requerido"
            Valido = false
            Toast.makeText(this@MainActivity, "Password vacio", Toast.LENGTH_LONG).show()

        } else binding.edtP.error = null

        return Valido
    }

    private fun obtenerInicioSesion() {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://bee-production.up.railway.app/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        //Toast.makeText(this,email+password , Toast.LENGTH_SHORT).show();
        var service: ApiService = retrofit.create(ApiService::class.java)
        service.postLogin(Login(binding.edtE.text.toString(), binding.edtP.text.toString()))
            .enqueue(object : Callback<ResponseResult> {
                override fun onResponse(
                    call: Call<ResponseResult>,
                    response: Response<ResponseResult>
                ) {
                    Log.e("onResponse", response.body().toString())
                    Toast.makeText(this@MainActivity, "Inicio exitoso" + response.body().toString(), Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(call: Call<ResponseResult>, t: Throwable) {
                    Log.e("onFailure", t.toString())
                }
            })
    }


    }
