package Interfases
import Models.ResponseResult
import Models.Login
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
interface ApiService {

    @POST("api/login")
    fun postLogin(@Body login: Login): Call<ResponseResult>


}